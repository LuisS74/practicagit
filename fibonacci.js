let resultado = fibonacci(10)

console.log(resultado)

function fibonacci(posicion){
    if(posicion < 1){
        return posicion
    }else{
        return fibonacci(posicion - 1) + fibonacci(posicion - 2)
    }
}